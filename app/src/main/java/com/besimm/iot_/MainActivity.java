package com.besimm.iot_;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {


    private Firebase firebase;
    Handler bluetoothIn;
    private ProgressBar pbSicaklik;
    private int pbValue = 0;
    final int handlerState = 0;
    private ConnectedThread mConnectedThread;
    private StringBuilder recDataString = new StringBuilder();

    TextView tvSicaklik, tvSonTarih;

    private static final String TAG = " ";
    Button btnBluetoothKontrol, btnFireBaseVerisiGonder, btnFireBaseVerisiAl, btnBaglan;
    EditText edtVeri;
    BluetoothAdapter adapter;
    private BluetoothSocket btSocket = null;
    private OutputStream outStream = null;
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static String address = "20:13:08:23:09:14";

    ProgressDialog progressDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Firebase.setAndroidContext(getApplicationContext());
        firebase = new Firebase("https://data-e62a7.firebaseio.com/");

        adapter = BluetoothAdapter.getDefaultAdapter();

        btnBluetoothKontrol = (Button) findViewById(R.id.btnBluetoothKontrol);

        btnFireBaseVerisiGonder = (Button) findViewById(R.id.btnFireBaseVerisiGonder);
        btnFireBaseVerisiAl = (Button) findViewById(R.id.btnFireBaseVerisiAl);
        btnBaglan = (Button) findViewById(R.id.btnBaglan);
        edtVeri = (EditText) findViewById(R.id.edtVeri);

        tvSicaklik = (TextView) findViewById(R.id.tvSicaklik);
        pbSicaklik = (ProgressBar) findViewById(R.id.pbSicaklik);
        tvSonTarih = (TextView) findViewById(R.id.tvSonTarih);


        pbSicaklik.setMax(50);
        pbSicaklik.setIndeterminate(false);

        if (!adapter.isEnabled()) {
            btnBaglan.setEnabled(false);
            btnFireBaseVerisiGonder.setEnabled(false);
        }
        bluetoothIn = new Handler() {


            public void handleMessage(android.os.Message msg) {


                if (msg.what == handlerState) {
                    //if message is what we want
                    String readMessage = (String) msg.obj;                                                                // msg.arg1 = bytes from connect thread
                    recDataString.append(readMessage);                                    //keep appending to string until ~
                    int endOfLineIndex = recDataString.indexOf("~");
                    // determine the end-of-line
                    if (endOfLineIndex > 0) {
                        if (recDataString.charAt(0) == '#')                                //if it starts with # we know it is what we are looking for
                        {
                            tvSicaklik.setText(recDataString.substring(1, 6) + " C");
                            pbValue = (int) Float.parseFloat(recDataString.substring(1, 6));
                            pbSicaklik.setProgress(pbValue);
                            recDataString.delete(0, recDataString.length());

                        }


                    }


                }
            }


        };

        btnBaglan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new CustomTask().execute((Void) null);

            }
        });


        btnBluetoothKontrol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (adapter == null) {
                    Toast.makeText(getApplicationContext(), "adapter null", Toast.LENGTH_LONG).show();
                } else {
                    if (adapter.isEnabled()) {
                        Toast.makeText(getApplicationContext(), "Bluetooth Açık", Toast.LENGTH_LONG).show();
                    } else {

                        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(enableBtIntent, 1);
                    }
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        btnBaglan.setEnabled(true);
                        btnFireBaseVerisiGonder.setEnabled(true);
                    }
                }, 2000);


            }
        });


        btnFireBaseVerisiGonder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                firebase.addChildEventListener(new com.firebase.client.ChildEventListener() {
                    @Override
                    public void onChildAdded(com.firebase.client.DataSnapshot dataSnapshot, String s) {


                        String veri = dataSnapshot.getValue(String.class);
                        final long time = System.currentTimeMillis();
                        long zaman = Long.parseLong(veri);
                        if (zaman < (time - 30000)) {
                            Firebase mRefChild = firebase.child("veri");
                            mRefChild.setValue(time);
                            SendData("0");

                        } else {
                            Toast.makeText(getApplicationContext(), "Son 30 Saniye içinde zaten yem verilmiş!", Toast.LENGTH_LONG).show();

                        }


                    }

                    @Override
                    public void onChildChanged(com.firebase.client.DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(com.firebase.client.DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(com.firebase.client.DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {

                    }
                });


            }
        });

        btnFireBaseVerisiAl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Firebase mRefChild = firebase.child("veri");
                //   mRefChild.setValue("15.10.2016"+random.nextInt(55));

                //  firebase.push().setValue("i" + random.nextInt(55));

                //  com.firebase.client.Query isim = firebase.child("veri");

                firebase.addChildEventListener(new com.firebase.client.ChildEventListener() {
                    @Override
                    public void onChildAdded(com.firebase.client.DataSnapshot dataSnapshot, String s) {


                        String tarih = dataSnapshot.getValue(String.class);
                        String dateFormat = "dd-MM-yyyy HH:mm";
                        Calendar calendar = Calendar.getInstance();
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
                        calendar.setTimeInMillis(Long.parseLong(tarih));


                        tvSonTarih.setText("Son Yemleme Tarihi: " + simpleDateFormat.format(calendar.getTime()));


                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }


                    @Override
                    public void onChildRemoved(com.firebase.client.DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(com.firebase.client.DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {

                    }
                });


            }
        });


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {
        if (Build.VERSION.SDK_INT >= 10) {
            try {
                final Method m = device.getClass().getMethod("createInsecureRfcommSocketToServiceRecord", UUID.class);
                return (BluetoothSocket) m.invoke(device, MY_UUID);
            } catch (Exception e) {
                Log.e(TAG, "Could not create Insecure RFComm Connection", e);
            }
        }
        return device.createRfcommSocketToServiceRecord(MY_UUID);
    }


    private void SendData(String message) {
        byte[] msgBuffer = message.getBytes();

        Log.d(TAG, "...Send data: " + message + "...");

        try {
            outStream.write(msgBuffer);
        } catch (IOException e) {
            String msg = "In onResume() and an exception occurred during write: " + e.getMessage();
            if (address.equals("00:00:00:00:00:00"))
                msg = msg + ".\n\nUpdate your server address from 00:00:00:00:00:00 to the correct address on line 35 in the java code";
            msg = msg + ".\n\nCheck that the SPP UUID: " + MY_UUID.toString() + " exists on server.\n\n";


        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class ConnectedThread extends Thread {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        //creation of the connect thread
        public ConnectedThread(BluetoothSocket socket) {
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                //Create I/O streams for connection
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }


        public void run() {
            byte[] buffer = new byte[256];
            int bytes;

            // Keep looping to listen for received messages
            while (true) {
                try {
                    bytes = mmInStream.read(buffer);            //read bytes from input buffer
                    String readMessage = new String(buffer, 0, bytes);
                    // Send the obtained bytes to the UI Activity via handler
                    bluetoothIn.obtainMessage(handlerState, bytes, -1, readMessage).sendToTarget();
                } catch (IOException e) {
                    break;
                }
            }
        }

        //write method
        public void write(String input) {
            byte[] msgBuffer = input.getBytes();           //converts entered String into bytes
            try {
                mmOutStream.write(msgBuffer);                //write bytes over BT connection via outstream
            } catch (IOException e) {
                //if you cannot write, close the application
                Toast.makeText(getBaseContext(), "Connection Failure", Toast.LENGTH_LONG).show();
                finish();

            }
        }
    }

    private class CustomTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setTitle("Ardunio UNO Bağlantısı");
            progressDialog.setMessage("HC-06 ile Bağlantı Kuruluyor...");
            progressDialog.setProgress(1);

            progressDialog.show();
        }

        protected Void doInBackground(Void... param) {
            BluetoothDevice device = adapter.getRemoteDevice(address);

            try {
                btSocket = createBluetoothSocket(device);
            } catch (IOException e) {
                e.printStackTrace();
            }
            adapter.cancelDiscovery();

            try {
                outStream = btSocket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                if (!btSocket.isConnected()) {
                    btSocket.connect();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            mConnectedThread = new ConnectedThread(btSocket);
            mConnectedThread.start();
            return null;
        }

        protected void onPostExecute(Void param) {

            progressDialog.dismiss();
        }
    }
}
